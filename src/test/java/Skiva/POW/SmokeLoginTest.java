package Skiva.POW;

import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import library.CaptureScreen;
import library.DriverSelection;
import pages.HomePage;
import pages.LoginPage;
import pages.admin.Navigation;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeClass;

import java.lang.reflect.Method;
import java.util.Date;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;

public class SmokeLoginTest {
	WebDriver driver;
	ExtentReports report;
	ExtentTest logger;

	Navigation nav;
	/*
  @Test(dataProvider = "dp")
  public void f(Integer n, String s) {
  }
  @DataProvider
  public Object[][] dp() {
    return new Object[][] {
      new Object[] { 1, "a" },
      new Object[] { 2, "b" },
    };
  }*/
	
	@DataProvider
	public Object[][] login(){
		return null;
	}
  
  @BeforeMethod (alwaysRun = true)
  public void beforeMethod(Method method) {
	  logger = report.startTest(String.format("%1s %2$s", "Admin", method.getName()));
  }

  @AfterMethod (alwaysRun = true)
  public void afterMethod(ITestResult result) {
		if (result.isSuccess()) {
			logger.log(LogStatus.PASS, "logs:");
		} else {
			logger.log(LogStatus.FAIL, "logs:");
			Date date = new Date();
			CaptureScreen screen = new library.CaptureScreen();
			screen.takeScreenShot(driver, result.getName() + "_" + date.getTime());

			if (screen.getPath() != null) {
				logger.log(LogStatus.INFO, logger.addScreenCapture("../../" + screen.getPath()));
			}
		}
		report.endTest(logger);
		report.flush();
  }


  
  @BeforeClass (alwaysRun = true)
  public void beforeClass() {
	  	driver = DriverSelection.getDefaultDriver();

		String reportFile = "./Reports/Current/adminSmokeResults.html";
		report = new ExtentReports(reportFile);
		nav = new Navigation(driver);
	  
  }

  @AfterClass (alwaysRun = true)
  public void afterClass() {
	  driver.close();
  }
  
	@Test(groups = { "login", "shoplogin" })
	public void goShopLogin() {
		driver.get("http://104.238.103.200:8080/POWNew");
		new pages.HomePage(driver).clickShop();
		Assert.assertEquals(driver.getTitle(), "POW Shop | Log in");
	}

	@Test(groups = { "login", "shoplogin" }, dependsOnMethods = "goShopLogin")
	public void loginShop() {
		new pages.LoginPage(driver).login("pow_s32@yahoo.com", "shop");
		Assert.assertEquals(driver.getTitle(), "POW Shop | Dashboard");
	}
	
	@Test(groups = { "login", "adminlogin" }, dependsOnGroups = "shoplogin")
	public void goAdminLogin() {
		driver.get("http://104.238.103.200:8080/POWNew");
		logger.log(LogStatus.INFO, "Test Case 01");
		new HomePage(driver).clickAdmin();
		Assert.assertEquals(driver.getTitle(), "POW Admin | Log in");
	}

	@Test(groups = { "login", "adminlogin" }, dependsOnMethods = "goAdminLogin")
	public void loginAdmin() {
		LoginPage pgLogin = new pages.LoginPage(driver);
		pgLogin.login("pow_a32@yahoo.com", "powadmin");
		Assert.assertEquals(driver.getTitle(), "POW Admin | Dashboard");
	}
	
	@Test(groups = { "login", "accountlogin" }, dependsOnGroups = "adminlogin")
	public void goAccountLogin() {
		driver.get("http://104.238.103.200:8080/POWNew");
		new pages.HomePage(driver).clickAccount();
		Assert.assertEquals(driver.getTitle(), "POW Account | Log in");
	}

	@Test(groups = { "login", "accountlogin" }, dependsOnMethods = "goAccountLogin")
	public void loginAccount() {
		new pages.LoginPage(driver).login("pow_ac32@yahoo.com", "account");
		Assert.assertEquals(driver.getTitle(), "POW Account | Dashboard");
	}

}
