package Skiva.POW;

import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import library.CaptureScreen;
import library.DriverSelection;
import pages.account.Navigation;

import java.lang.reflect.Method;
import java.util.Date;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

public class SmokeAccountTest {
	WebDriver driver;
	ExtentReports report;
	ExtentTest logger;

	Navigation nav;

	@AfterClass (alwaysRun = true)
	public void afterClass() {
		driver.close();
	}

	@BeforeClass (alwaysRun = true)
	public void beforeClass() {
		driver = DriverSelection.getDefaultDriver();
		driver.get("http://104.238.103.200:8080/POWNew");

		String reportFile = "./Reports/Current/accountSmokeResults.html";
		report = new ExtentReports(reportFile);
		nav = new Navigation(driver);
		
		goAccountLogin();
		loginAccount();
	}

	@BeforeMethod (alwaysRun = true)
	public void startLogger(Method method) {
		logger = report.startTest(String.format("%1s %2$s", "Account", method.getName()));
	}

	@AfterMethod (alwaysRun = true)
	public void logInfo(ITestResult result) {
		if (result.isSuccess()) {
			logger.log(LogStatus.PASS, "logs:");
		} else {
			logger.log(LogStatus.FAIL, "logs:");
			Date date = new Date();
			CaptureScreen screen = new library.CaptureScreen();
			screen.takeScreenShot(driver, result.getName() + "_" + date.getTime());

			if (screen.getPath() != null) {
				logger.log(LogStatus.INFO, logger.addScreenCapture("../../" + screen.getPath()));
			}
		}
		report.endTest(logger);
		report.flush();
	}

	public void goAccountLogin() {
		new pages.HomePage(driver).clickAccount();
		Assert.assertEquals(driver.getTitle(), "POW Account | Log in");
	}

	public void loginAccount() {
		new pages.LoginPage(driver).login("pow_ac32@yahoo.com", "account");
		Assert.assertEquals(driver.getTitle(), "POW Account | Dashboard");
	}

	@Test(groups = { "navigation","vendor" })
	public void clickVendorManagementNav() {
		nav.clickVendorManagementNav();
		Assert.assertEquals(driver.getTitle(), "POW Account | Vendor Management");
	}

	@Test(groups = { "navigation","PO","invoice" })
	public void clickPOInvoiceManagementNav() {
		nav.clickPOInvoiceManagementNav();
		Assert.assertEquals(driver.getTitle(), "POW Account | Invoice Management");
	}

	@Test(groups = { "navigation","reports" })
	public void clickVendorReports() {
		nav.clickVendorsReports();
		Assert.assertEquals(driver.getTitle(), "POW Account | Reports | Vendor Reports");
	}

	@Test(groups = { "navigation","reports" })
	public void clickPOReports() {
		nav.clickPOReports();
		Assert.assertEquals(driver.getTitle(), "POW Account | Reports | PO Reports");
	}

	@Test(groups = { "navigation","reports" })
	public void clickInvoicesReports() {
		nav.clickInvoicesReports();
		Assert.assertEquals(driver.getTitle(), "POW Account | Reports | Invoice Reports");
	}

	@Test(groups = { "navigation","messages" })
	public void clickMessagesNav() {
		nav.clickMessagesNav();
		Assert.assertEquals(driver.getTitle(), "POW Account | Messages");
	}

	@Test(groups = { "messages" })
	public void clickComposeMessage() {
		clickMessagesNav();
		new pages.account.MessagesPage(driver).clickComposeMessage();
		Assert.assertEquals(new pages.account.ComposeMessagePage(driver).getCaption(), "COMPOSE MESSAGE");
	}

	@Test(groups = { "messages" })
	public void clickComposeMessageFail() {
		clickMessagesNav();
		new pages.account.MessagesPage(driver).clickComposeMessage();
		Assert.assertEquals(new pages.account.ComposeMessagePage(driver).getCaption(), "COMPOSE MESSAGE FAIL");
	}
}
