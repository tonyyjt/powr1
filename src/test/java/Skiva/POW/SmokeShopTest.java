package Skiva.POW;

import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import library.CaptureScreen;
import library.DriverSelection;
import pages.shop.Navigation;
import pages.shop.ReportsPage;

import java.lang.reflect.Method;
import java.util.Date;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

public class SmokeShopTest {
	WebDriver driver;
	ExtentReports report;
	ExtentTest logger;

	Navigation nav;

	@AfterClass (alwaysRun = true)
	public void afterMethod() {
		driver.close();
	}

	@BeforeClass (alwaysRun = true)
	public void beforeClass() {
		driver = DriverSelection.getDefaultDriver();
		driver.get("http://104.238.103.200:8080/POWNew");

		String reportFile = "./Reports/Current/shopSmokeResults.html";
		report = new ExtentReports(reportFile);
		nav = new Navigation(driver);
		
		goShopLogin();
		loginShop();
	}

	@BeforeMethod (alwaysRun = true)
	public void startLogger(Method method) {
		logger = report.startTest(String.format("%1s %2$s", "Shop", method.getName()));
	}

	@AfterMethod (alwaysRun = true)
	public void logInfo(ITestResult result) {
		if (result.isSuccess()) {
			logger.log(LogStatus.PASS, "logs:");
		} else {
			logger.log(LogStatus.FAIL, "logs:");
			Date date = new Date();
			CaptureScreen screen = new library.CaptureScreen();
			screen.takeScreenShot(driver, result.getName() + "_" + date.getTime());

			if (screen.getPath() != null) {
				logger.log(LogStatus.INFO, logger.addScreenCapture("../../" + screen.getPath()));
			}
		}
		report.endTest(logger);
		report.flush();
	}

	public void goShopLogin() {
		new pages.HomePage(driver).clickShop();
		Assert.assertEquals(driver.getTitle(), "POW Shop | Log in");
	}

	public void loginShop() {
		new pages.LoginPage(driver).login("pow_s32@yahoo.com", "shop");
		Assert.assertEquals(driver.getTitle(), "POW Shop | Dashboard");
	}

	@Test(groups = { "navigation","POR" })
	public void PORManagementNav() {
		nav.clickPORManagementNav();
		Assert.assertEquals(driver.getTitle(), "POW Shop | POR Management");
	}

	@Test(groups = { "POR" }, dependsOnMethods = "PORManagementNav")
	public void createPORequest() {
		PORManagementNav();
		new pages.shop.PORManagementPage(driver).clickCreatePORequest();
		Assert.assertEquals(new pages.shop.AddPORPage(driver).getCaption(), "POR ADD");
	}

	@Test(groups = { "navigation","reports" })
	public void dailyReportNav() {
		nav.clickDailyReport();
		Assert.assertEquals(new ReportsPage(driver).getInnerPageTitle(), "Daily POR Report");
	}

	@Test(groups = { "navigation","reports" })
	public void weeklyReportNav() {
		nav.clickWeeklyReport();
		Assert.assertEquals(new ReportsPage(driver).getInnerPageTitle(), "Weekly POR Report");
	}

	@Test(groups = { "navigation","reports" })
	public void monthlyReportNav() {
		nav.clickMonthlyReport();
		Assert.assertEquals(new ReportsPage(driver).getInnerPageTitle(), "Monthly POR Report");
	}

	@Test(groups = { "navigation","reports" })
	public void itemBasedReportNav() {
		nav.clickItemBasedReport();
		Assert.assertEquals(new ReportsPage(driver).getInnerPageTitle(), "Item Based POR Report");
	}

	@Test(groups = { "navigation", "messages" })
	public void clickMessagesNav() {
		nav.clickMessagesNav();
		Assert.assertEquals(driver.getTitle(), "POW Shop | Messaging");
	}

	@Test(groups = { "messages" }, dependsOnMethods = "clickMessagesNav")
	public void clickComposeMessageButton() {
		clickMessagesNav();
		new pages.shop.MessagesPage(driver).clickComposeMessage();
		Assert.assertEquals(new pages.shop.ComposeMessagePage(driver).getCaption(), "COMPOSE MESSAGE");
	}
}
