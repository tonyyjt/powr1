package Skiva.POW;

import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import library.CaptureScreen;
import library.DriverSelection;
import pages.HomePage;
import pages.LoginPage;
import pages.admin.ItemManagementPage;
import pages.admin.Navigation;
import pages.admin.ReportsPage;
import pages.admin.VendorManagementPage;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Date;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

public class SmokeAdminTest {
	WebDriver driver;
	ExtentReports report;
	ExtentTest logger;

	Navigation nav;

	@AfterClass (alwaysRun = true)
	public void afterMethod() {
		driver.close();
	}

	@DataProvider
	public Object[][] dp() {
		return new Object[][] { new Object[] { 1, "a" }, new Object[] { 2, "b" }, };
	}

	@BeforeClass (alwaysRun = true)
	public void beforeClass() throws IOException {
		driver = DriverSelection.getDefaultDriver();
		driver.get("http://104.238.103.200:8080/POWNew");

		String reportFile = "./Reports/Current/adminSmokeResults.html";
		report = new ExtentReports(reportFile);
		nav = new Navigation(driver);
		
		goAdminLogin();
		loginAdmin();
		/*
		 * File file = new File("../datadocs/facebook.ods"); Sheet sheet =
		 * SpreadSheet.createFromFile(file).getSheet(0); ReadData read = new
		 * library.ReadData(); row = read.getTotalRows(sheet);
		 * read.getValue(sheet, 0, 0);
		 */
	}

	@BeforeMethod (alwaysRun = true)
	public void startLogger(Method method) {
		logger = report.startTest(String.format("%1s %2$s", "Admin", method.getName()));
	}

	@AfterMethod (alwaysRun = true)
	public void logInfo(ITestResult result) {
		if (result.isSuccess()) {
			logger.log(LogStatus.PASS, "logs:");
		} else {
			logger.log(LogStatus.FAIL, "logs:");
			Date date = new Date();
			CaptureScreen screen = new library.CaptureScreen();
			screen.takeScreenShot(driver, result.getName() + "_" + date.getTime());

			if (screen.getPath() != null) {
				logger.log(LogStatus.INFO, logger.addScreenCapture("../../" + screen.getPath()));
			}
		}
		report.endTest(logger);
		report.flush();
	}

	public void goAdminLogin() {
		new HomePage(driver).clickAdmin();
		Assert.assertEquals(driver.getTitle(), "POW Admin | Log in");
	}

	public void loginAdmin() {
		LoginPage pgLogin = new pages.LoginPage(driver);
		pgLogin.login("pow_a32@yahoo.com", "powadmin");
		Assert.assertEquals(driver.getTitle(), "POW Admin | Dashboard");
	}

	@Test(groups = { "navigation","vendor" })
	public void clickVendorManagementNav() {
		nav.clickVendorManagement();
		Assert.assertEquals(driver.getTitle(), "POW Admin | Vendor Management");
	}

	@Test(groups = { "vendor" },dependsOnMethods = "clickVendorManagementNav")
	public void addVendor() {
		clickVendorManagementNav();
		new VendorManagementPage(driver).clickAddVendor();
		Assert.assertEquals(new pages.admin.AddVendorPage(driver).getCaption(), "ADD VENDOR");
	}

	@Test(groups = { "navigation","item" })
	public void clickItemManagementNav() {
		nav.clickItemManagement();
		Assert.assertEquals(driver.getTitle(), "POW Admin | Item Management");
	}

	@Test(groups = { "item" },dependsOnMethods = "clickItemManagementNav")
	public void addItem() {
		clickItemManagementNav();
		new ItemManagementPage(driver).clickAddItem();
		Assert.assertEquals(new pages.admin.AddItemPage(driver).getCaption(), "ADD ITEM");
	}

	@Test(groups = { "navigation","POR" })
	public void clickPORManagementNav() {
		nav.clickPORManagement();
		Assert.assertEquals(driver.getTitle(), "POW Admin | POR Management");
	}

	@Test(groups = { "POR" },dependsOnMethods = "clickPORManagementNav")
	public void createPORequest() {
		clickPORManagementNav();
		new pages.admin.PORManagementPage(driver).clickCreatePORequest();
		Assert.assertEquals(new pages.admin.AddPORPage(driver).getCaption(), "ADD POR");
	}

	@Test(groups = { "navigation","PO" })
	public void clickPOManagementNav() {
		nav.clickPOManagement();
		Assert.assertEquals(driver.getTitle(), "POW Admin | PO Management");
	}

	@Test(groups = { "PO" },dependsOnMethods = "clickPOManagementNav")
	public void createNewPO() {
		clickPOManagementNav();
		new pages.admin.POManagementPage(driver).clickCreateNewPO();
		Assert.assertEquals(new pages.admin.GeneratePOPage(driver).getCaption(), "GENERATE PO");
	}

	@Test(groups = { "navigation","invoice" })
	public void clickInvoicesNav() {
		nav.clickInvoices();
		Assert.assertEquals(driver.getTitle(), "POW Admin | Invoice Management");
	}

	@Test(groups = { "invoice" },dependsOnMethods = "clickInvoicesNav")
	public void clickEnterVendorInvoice() {
		clickInvoicesNav();
		new pages.admin.InvoiceManagementPage(driver).clickEnterVendorInvoice();
		Assert.assertEquals(new pages.admin.AddInvoicePage(driver).getCaption(), "ADD INVOICE");
	}

	@Test(groups = { "navigation","reports" })
	public void POReport() {
		nav.clickPOReports();
		Assert.assertEquals(new ReportsPage(driver).getInnerPageTitle(), "PO Report");
	}

	@Test(groups = { "navigation","reports" })
	public void InvoicesReport() {
		nav.clickInvoicesReports();
		Assert.assertEquals(new ReportsPage(driver).getInnerPageTitle(), "Invoice Report");
	}

	@Test(groups = { "navigation","reports" })
	public void VendorsReport() {
		nav.clickVendorsReports();
		Assert.assertEquals(new ReportsPage(driver).getInnerPageTitle(), "Vendor Report");
	}

	@Test(groups = { "navigation","reports" })
	public void ItemsReport() {
		nav.clickItemsReports();
		Assert.assertEquals(new ReportsPage(driver).getInnerPageTitle(), "Item Report");
	}

	@Test(groups = { "messages", "navigation" })
	public void clickMessagesNav() {
		nav.clickMessages();
		Assert.assertEquals(driver.getTitle(), "POW Admin | Messages");
	}

	@Test(groups = { "messages" },dependsOnMethods = "clickMessagesNav")
	public void clickComposeMessage() {
		clickMessagesNav();
		new pages.admin.MessagePage(driver).clickComposeMessage();
		Assert.assertEquals(new pages.admin.ComposeMessagePage(driver).getCaption(), "COMPOSE MESSAGE");
	}
}
