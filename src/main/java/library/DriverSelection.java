package library;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;

public class DriverSelection {
	public static WebDriver getDefaultDriver(){
		return new DriverSelection().getChromeDriver();
	}
	
	public WebDriver getChromeDriver(){
		System.setProperty("webdriver.chrome.driver", "../chromedriver.exe");
		return new ChromeDriver();
	}
	
	public WebDriver getPhantomDriver(){
		System.setProperty("webdriver.phantomjs.driver", "../phantomjs.exe");
		return new PhantomJSDriver();
	}
}
