package library;

import org.jopendocument.dom.spreadsheet.Cell;
import org.jopendocument.dom.spreadsheet.MutableCell;
import org.jopendocument.dom.spreadsheet.Sheet;

public class ReadDataOds {
    /*
     * -----------------OPEN SPREADSHEET--------------------------
     */
    public MutableCell<?> getValue(Sheet sheet, int row, int cell){
    	return sheet.getCellAt(cell, row);
    }
    
    public int getTotalRows(Sheet sheet, int headerRowCount){
    	return getTotalRows(sheet) - headerRowCount;
    }
    
    public int getTotalRows(Sheet sheet){
    	return sheet.getRowCount();
    }
    
    public Cell<?>[] getColumnArr(Sheet sheet, int col, int noOfRows){
    	int totalRows = getTotalRows(sheet);
    	Cell<?>[] value = new Cell<?>[totalRows];
    	for(int i=0; i<totalRows; i++){
    		value[i] = sheet.getCellAt(i, col);
    	}
    	return value;
    }
    
    public Cell<?>[][] getRowsWithValueAtColumn(Sheet sheet, String value, int col, int columnsToGet){
    	
    	Cell<?>[] column = getColumnArr(sheet, col, sheet.getRowCount());
    	int rowsToGet = getMatchCountInCellArr(column, value);
    	
    	if(rowsToGet == 0){
    		return null;
    	}
    	
    	Cell<?>[][] matchingRows = new Cell<?>[rowsToGet][columnsToGet];
    	for(int i = 0; i < rowsToGet; i++){
    		for(int j = 0; j < columnsToGet; j++){
    			matchingRows[i][j] = sheet.getCellAt(i, j);
    		}
    	}
    	
    	return matchingRows;
    }
    
    public int getMatchCountInCellArr(Cell<?>[] column, String value){
    	int rows = 0;
    	for(int i=0; i<column.length; i++){
    		if(value.equals(column[i])){
    			rows++;
    		}
    	}
    	return rows;
    }
}
