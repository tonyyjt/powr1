package library;


import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;

public class ReadDataXls 
{
    
    public XSSFCell getValue(XSSFSheet sheet, int row, int cell){
    	return sheet.getRow(row).getCell(cell);
    }
    
    public int getTotalRows(XSSFSheet sheet, int headerRowCount){
    	return getTotalRows(sheet) - headerRowCount;
    }
    
    public int getTotalRows(XSSFSheet sheet){
    	return sheet.getLastRowNum() + 1;
    }
    
    public int getLastRowIndex(XSSFSheet sheet){
    	return sheet.getLastRowNum();
    }

}
