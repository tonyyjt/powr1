package library;

import java.io.File;
import java.io.IOException;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.google.common.io.Files;

public class CaptureScreen {
	String path = "";
	
	public void takeScreenShot(WebDriver driver, String name) {
		TakesScreenshot ss = (TakesScreenshot) driver;
		File file = new File("screenshots");
		if (!file.exists()) {
			file.mkdir();
		}
		
		path = "screenshots/" + name +".png";
		
		File output = new File(path);

		File input = ss.getScreenshotAs(OutputType.FILE);
		try {
			Files.copy(input, output);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String getPath(){
		if(!path.isEmpty()){
			return path;
		}
		else{
			return null;
		}
	}
}
