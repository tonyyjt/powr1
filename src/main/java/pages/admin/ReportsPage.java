package pages.admin;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ReportsPage {
	WebDriver driver;
	
	public ReportsPage(WebDriver driver){
		this.driver = driver;
	}
	
	public String getInnerPageTitle(){
		return driver.findElement(By.cssSelector("h3.page-title")).getText();
	}
}
