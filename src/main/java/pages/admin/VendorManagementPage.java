package pages.admin;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class VendorManagementPage {
	public static void main(String args[]){
		System.setProperty("webdriver.chrome.driver", "../chromedriver");
		WebDriver driver = new ChromeDriver();
		driver.get("http://104.238.103.200:8080/POWNew");
		new pages.HomePage(driver).clickAdmin();
		new pages.LoginPage(driver).login("pow_a32@yahoo.com", "powadmin");
		new Navigation(driver).clickVendorManagement();
		
		VendorManagementPage page = new VendorManagementPage(driver);
		page.clickAddVendor();
	}
	
	WebDriver driver;
	public VendorManagementPage(WebDriver driver){
		this.driver = driver;
	}
	
	public void clickAddVendor(){
		driver.findElement(By.id("addVendor")).click();
	}
}
