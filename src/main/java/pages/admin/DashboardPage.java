package pages.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DashboardPage{
	WebDriver driver;
	static WebDriverWait wait;
	
	public static void main(String[] args){
		System.setProperty("webdriver.chrome.driver", "../chromedriver");
		WebDriver driver = new ChromeDriver();
		driver.get("http://104.238.103.200:8080/POWNew");
		Navigation dash = new Navigation(driver);
		
		new pages.HomePage(driver).clickAdmin();
		new pages.LoginPage(driver).login("pow_a32@yahoo.com", "powadmin");
		dash.clickPOReports();
		
		//wait = new WebDriverWait(driver, 10);
		//wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("h3.page-title")));
		
		System.out.println(new ReportsPage(driver).getInnerPageTitle());
	}
	
	DashboardPage(WebDriver driver){
		this.driver = driver;
	}
}
