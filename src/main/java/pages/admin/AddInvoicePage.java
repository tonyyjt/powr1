package pages.admin;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AddInvoicePage {
WebDriver driver;
	
	public AddInvoicePage(WebDriver driver){
		this.driver = driver;
	}
	
	public String getInnerPageTitle(){
		return driver.findElement(By.cssSelector("h3.page-title")).getText();
	}
	
	public String getCaption(){
		return driver.findElement(By.cssSelector("span.caption-subject")).getText();
	}
}
