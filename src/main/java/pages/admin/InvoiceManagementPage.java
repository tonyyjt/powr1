package pages.admin;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class InvoiceManagementPage {
	WebDriver driver;
	public InvoiceManagementPage(WebDriver driver){
		this.driver = driver;
	}
	
	public void clickEnterVendorInvoice(){
		driver.findElement(By.id("addVendor")).click();
	}
}
