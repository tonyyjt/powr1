package pages.admin;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class Navigation {
	WebDriver driver;
	
	public Navigation(WebDriver driver){
		this.driver = driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

	public void clickVendorManagement() {
		driver.findElement(By.linkText("Vendor Management")).click();
	}

	public void clickItemManagement() {
		driver.findElement(By.linkText("Item Management")).click();
	}

	public void clickPORManagement() {
		driver.findElement(By.linkText("POR Management")).click();
	}

	public void clickPOManagement() {
		driver.findElement(By.linkText("PO Management")).click();
	}

	public void clickInvoices() {
		driver.findElement(By.linkText("Invoices")).click();
	}

	public void clickMessages() {
		driver.findElement(By.linkText("Messages")).click();
	}
	
	public void clickPOReports(){
		new Actions(driver).moveToElement(driver.findElement(By.linkText("Reports")))
		.moveToElement(driver.findElement(By.cssSelector("a[href='/POWNew/admin/poreport.do']")))
		.click().build().perform();
	}
	
	public void clickInvoicesReports(){
		new Actions(driver).moveToElement(driver.findElement(By.linkText("Reports")))
		.moveToElement(driver.findElement(By.cssSelector("a[href='/POWNew/admin/invoicereport.do']"))).click()
		.build().perform();
	}
	
	public void clickVendorsReports(){
		new Actions(driver).moveToElement(driver.findElement(By.linkText("Reports")))
		.moveToElement(driver.findElement(By.cssSelector("a[href='/POWNew/admin/vendorreport.do']"))).click()
		.build().perform();
	}
	
	public void clickItemsReports(){
		new Actions(driver).moveToElement(driver.findElement(By.linkText("Reports")))
		.moveToElement(driver.findElement(By.cssSelector("a[href='/POWNew/admin/itemreport.do']"))).click()
		.build().perform();
	}
}
