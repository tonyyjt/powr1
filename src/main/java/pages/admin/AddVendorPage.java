package pages.admin;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AddVendorPage {
WebDriver driver;
	
	public AddVendorPage(WebDriver driver){
		this.driver = driver;
	}
	
	/*
	 * ===============================RETRIEVAL=================================================
	 */
	
	public String getInnerPageTitle(){
		return driver.findElement(By.cssSelector("h3.page-title")).getText();
	}
	
	public String getCaption(){
		return driver.findElement(By.cssSelector("span.caption-subject")).getText();
	}
	
	/*
	 * ==================================FORM CONTROLS============================================
	 */
	
	public void inputVendorName(String name){
		driver.findElement(By.id("vendorName")).sendKeys(name);
	}
	
	public void inputContactPerson(String name){
		driver.findElement(By.id("contactPerson")).sendKeys(name);
	}
	
	public void inputEmail(String email){
		driver.findElement(By.id("email")).sendKeys(email);
	}
	
	public void inputAccountNumber(String number){
		driver.findElement(By.id("accountNumber")).sendKeys(number);
	}
	
	public void inputBankDetail(String detail){
		driver.findElement(By.id("bankDetail")).sendKeys(detail);
	}
	
	public void inputAddress(String address){
		driver.findElement(By.id("address")).sendKeys(address);
	}
	
	public void inputPhoneNumber(String number){
		driver.findElement(By.id("phoneNumber")).sendKeys(number);
	}
	
	public void inputCity(String city){
		driver.findElement(By.id("city")).sendKeys(city);
	}
	
	public void inputState(String state){
		driver.findElement(By.id("state")).sendKeys(state);
	}
	
	public void inputCountry(String country){
		driver.findElement(By.id("country")).sendKeys(country);
	}
	
	public void inputZipCode(String code){
		driver.findElement(By.id("zipcode")).sendKeys(code);
	}
	
	public void clickSubmitButton(){
		driver.findElement(By.xpath("//button[text()='Submit']")).click();
	}
	
	public void clickCancelButton(){
		driver.findElement(By.id("cancelButton")).click();
	}
	
	/*
	 * ======================================MULTI-ACTIONS========================================
	 */
	
	public void fillForm(String[] values){
		
	}
}
