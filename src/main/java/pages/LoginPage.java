package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {
	WebDriver driver;
	public LoginPage(WebDriver driver){
		this.driver = driver;
	}
	
	public void inputEmail(String email){
		driver.findElement(By.id("email")).sendKeys(email);
	}
	
	public void inputPassword(String password){
		driver.findElement(By.id("password")).sendKeys(password);
	}
	
	public void clickLogin(){
		driver.findElement(By.id("loginbutton")).click();
	}
	
	public void login(String email, String password){
		inputEmail(email);
		inputPassword(password);
		clickLogin();
	}
}
