package pages.shop;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import pages.shop.Navigation;

public class DashboardPage {

	public static void main(String[] args){
		System.setProperty("webdriver.chrome.driver", "../chromedriver");
		WebDriver driver = new ChromeDriver();
		driver.get("http://104.238.103.200:8080/POWNew");
		Navigation dash = new Navigation(driver);
		
		new pages.HomePage(driver).clickShop();
		new pages.LoginPage(driver).login("pow_s32@yahoo.com", "shop");
		dash.clickPORManagementNav();
		new pages.shop.PORManagementPage(driver).clickCreatePORequest();
		System.out.println(new pages.shop.AddPORPage(driver).getInnerPageTitle());
	}
}
