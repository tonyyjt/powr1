package pages.shop;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class Navigation {
	WebDriver driver;
	
	public Navigation(WebDriver driver){
		this.driver = driver;
	}
	
	public void clickPORManagementNav(){
		driver.findElement(By.linkText("POR Management")).click();
	}
	
	public void clickDailyReport(){
		new Actions(driver).moveToElement(driver.findElement(By.linkText("Reports")))
		.moveToElement(driver.findElement(By.cssSelector("a[href='/POWNew/shop/dailyreport.do']")))
		.click().build().perform();
	}
	
	public void clickWeeklyReport(){
		new Actions(driver).moveToElement(driver.findElement(By.linkText("Reports")))
		.moveToElement(driver.findElement(By.cssSelector("a[href='/POWNew/shop/weeklyreport.do']")))
		.click().build().perform();
	}
	
	public void clickMonthlyReport(){
		new Actions(driver).moveToElement(driver.findElement(By.linkText("Reports")))
		.moveToElement(driver.findElement(By.cssSelector("a[href='/POWNew/shop/monthlyreport.do']")))
		.click().build().perform();
	}
	
	public void clickItemBasedReport(){
		new Actions(driver).moveToElement(driver.findElement(By.linkText("Reports")))
		.moveToElement(driver.findElement(By.cssSelector("a[href='/POWNew/shop/itembasedreport.do']")))
		.click().build().perform();
	}
	
	public void clickMessagesNav(){
		driver.findElement(By.linkText("Messages")).click();
	}
}
