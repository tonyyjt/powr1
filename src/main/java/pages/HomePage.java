package pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class HomePage {
	public static void main(String args[]){
		System.setProperty("webdriver.chrome.driver", "../chromedriver");
		WebDriver driver = new ChromeDriver();
		driver.get("http://104.238.103.200:8080/POWNew");
		
		new HomePage(driver).clickAdmin();
	}
	WebDriver driver;
	List<WebElement> userSelectionMenu = null;
	
	public HomePage(WebDriver driver){
		this.driver = driver;
		userSelectionMenu = driver.findElements(By.className("btn"));
	}
	
	public void clickAdmin(){
		userSelectionMenu.get(0).click();
	}
	
	public void clickShop(){
		userSelectionMenu.get(1).click();
	}
	
	public void clickAccount(){
		userSelectionMenu.get(2).click();
	}
	
	public void clickVendor(){
		userSelectionMenu.get(3).click();
	}
}
