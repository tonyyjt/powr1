package pages.account;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class Navigation {
	WebDriver driver;
	public Navigation(WebDriver driver){
		this.driver = driver;
	}
	
	public void clickVendorManagementNav(){
		driver.findElement(By.linkText("Vendor Management")).click();
	}
	
	public void clickPOInvoiceManagementNav(){
		driver.findElement(By.linkText("PO-Invoice Management")).click();
	}
	
	public void clickVendorsReports(){
		new Actions(driver).moveToElement(driver.findElement(By.linkText("Reports")))
		.moveToElement(driver.findElement(By.cssSelector("a[href='/POWNew/account/vendorreport.do']"))).click()
		.build().perform();
	}
	
	public void clickPOReports(){
		new Actions(driver).moveToElement(driver.findElement(By.linkText("Reports")))
		.moveToElement(driver.findElement(By.cssSelector("a[href='/POWNew/account/poreport.do']"))).click()
		.build().perform();
	}
	
	public void clickInvoicesReports(){
		new Actions(driver).moveToElement(driver.findElement(By.linkText("Reports")))
		.moveToElement(driver.findElement(By.cssSelector("a[href='/POWNew/account/invoicereport.do']"))).click()
		.build().perform();
	}
	
	public void clickMessagesNav(){
		driver.findElement(By.linkText("Messages")).click();
	}
}
